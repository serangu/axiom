package controllers

import (
	"strconv"
	"net/http"
	"github.com/julienschmidt/httprouter"
	m "bitbucket.org/serangu/axiom/models"
)

func IndexHandler(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	w.Write([]byte("Hello! This is a Start page!!!"))
}

func BookNameHandler(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
	var book m.Book
	book.Id, _ = strconv.Atoi(p.ByName("id"))
	w.Write([]byte(book.GetName()+" "+book.GetCost()))
}