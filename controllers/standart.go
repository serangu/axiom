package controllers

import (
	"net/http"
	"github.com/julienschmidt/httprouter"
	m "bitbucket.org/serangu/axiom/models"
)

// Соглашение
func AgreementHandler(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	// Переменная для хранения того, что будет отправлено в шаблон
	Data := make(map[interface{}]interface{})
	
	// 2 {{.a.Name}}
	Data["book"] = &m.Book{Id: 1, Title: "Book one", Cost: 11}
	
	m.Templates.ExecuteTemplate(w, "book", Data)
	
	//templates.ExecuteTemplate(w, "book", Data)
}

// Контакты
func ContactsHandler(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	w.Write([]byte("Страница с Контактами."))
}