package models

import (
	
)

type Book struct {
	Id int
	Title string
	Cost int
}

func (book Book) GetName() string {
	if book.Id == 1 {
		return "Book One"
	} else if book.Id == 2 {
		return "Book Two"
	} else {
		return "Book without name"
	}
}

func (book Book) GetCost() string {
	if book.Id == 1 {
		return "10"
	} else if book.Id == 2 {
		return "20"
	} else {
		return "FFF"
	}
}